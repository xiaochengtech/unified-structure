// 用户身份

/** 网页用户 */
export const Web = 1;
/** 微信用户 */
export const Wechat = 2;
/** 支付宝用户 */
export const Alipay = 3;

/** 合集 */
export const Set = [
    { value: Web, label: "网页用户" },
    { value: Wechat, label: "微信用户" },
    { value: Alipay, label: "支付宝用户" },
];