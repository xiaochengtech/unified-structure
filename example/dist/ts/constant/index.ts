import * as AuthorityModule from './custom';
import * as UserIdentity from './iota';

export {
    AuthorityModule,
    UserIdentity,
};