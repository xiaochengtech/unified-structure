// 模板表

import { CommonModel } from './common_model';

export interface Template extends CommonModel {
    /** ID编号 */
    id: number;
    /** 模板名称 */
    name: string;
    /** 金额(分) */
    amount: number;
    /** 附加属性 */
    custom: string;
}