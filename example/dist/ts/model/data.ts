// 附加属性

export interface Property {
    /** 用户类型 */
    ut: number;
    /** 在线时长，单位分钟 */
    ot: number;
}