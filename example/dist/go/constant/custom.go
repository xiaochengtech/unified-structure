// 权限模块
package constant

const (
	AuthorityModuleUser      = "user"      // 用户模块
	AuthorityModuleOperation = "operation" // 运营模块
)

func GetAuthorityModuleLabel(v string) string {
	switch v {
	case AuthorityModuleUser:
		return "用户模块"
	case AuthorityModuleOperation:
		return "运营模块"
	}
	return ""
}
