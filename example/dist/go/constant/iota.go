// 用户身份
package constant

const (
	UserIdentityWeb    uint8 = iota + 1 // 网页用户
	UserIdentityWechat                  // 微信用户
	UserIdentityAlipay                  // 支付宝用户
)

func GetUserIdentityLabel(v uint8) string {
	switch v {
	case UserIdentityWeb:
		return "网页用户"
	case UserIdentityWechat:
		return "微信用户"
	case UserIdentityAlipay:
		return "支付宝用户"
	}
	return ""
}
