// 附加属性
package model

type Property struct {
	UserType   uint8 `json:"ut"` // 用户类型
	OnlineTime int64 `json:"ot"` // 在线时长，单位分钟
}
