// 通用模型
package model

import (
	"time"
)

type CommonModel struct {
	CreateTime time.Time `json:"-" gorm:"type:datetime;default:CURRENT_TIMESTAMP"`                                   // 创建时间
	UpdateTime time.Time `json:"-" gorm:"type:datetime;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;index"` // 更新时间
}
