// 模板表
package model

import (
	"encoding/json"
)

const TemplateName = "template"

type Template struct {
	CommonModel
	ID     uint64 `json:"id" gorm:"auto_increment;primary_key"` // ID编号
	Name   string `json:"name" gorm:"not null;size:60"`         // 模板名称
	Amount int64  `json:"amount" gorm:"not null"`               // 金额(分)
	Custom string `json:"custom" gorm:"type:text"`              // 附加属性
}

func (Template) TableName() string {
	return TemplateName
}

func (m Template) GetCustom() (item Property, err error) {
	err = json.Unmarshal([]byte(m.Custom), &item)
	return
}
