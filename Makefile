CMD=go run main.go
BASE=`pwd`/example
SRC=$(BASE)/origin
DEST=$(BASE)/dist

.PHONY: build
build:
	rm -rf dist
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o dist/tool-linux -mod vendor main.go
	GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -o dist/tool-windows.exe -mod vendor main.go
	GOOS=darwin GOARCH=amd64 CGO_ENABLED=0 go build -o dist/tool-mac -mod vendor main.go

.PHONY: test
test: const-go const-ts model-go model-ts
	@echo "\n全部完成"

const-go:
	@echo "----------Constant-GO----------"
	@rm -rf $(DEST)/go/constant; mkdir -p $(DEST)/go/constant
	@$(CMD) -src $(SRC)/constant -dest $(DEST)/go/constant -language go
	@go fmt $(DEST)/go/constant > /dev/null

const-ts:
	@echo "----------Constant-JS----------"
	@rm -rf $(DEST)/ts/constant; mkdir -p $(DEST)/ts/constant
	@$(CMD) -src $(SRC)/constant -dest $(DEST)/ts/constant -language ts

model-go:
	@echo "----------Model-GO----------"
	@rm -rf $(DEST)/go/model; mkdir -p $(DEST)/go/model
	@$(CMD) -src $(SRC)/model -dest $(DEST)/go/model -language go
	@go fmt $(DEST)/go/model > /dev/null

model-ts:
	@echo "----------Constant-JS----------"
	@rm -rf $(DEST)/ts/model; mkdir -p $(DEST)/ts/model
	@$(CMD) -src $(SRC)/model -dest $(DEST)/ts/model -language ts
