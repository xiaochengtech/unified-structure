package util

import (
	"gitee.com/xiaochengtech/unified-structure/src/common"
)

// 是否是继承
func IsExtend(varType string) bool {
	return varType == common.VarTypeExtend
}

// 是否是引用
func IsReference(varType string) bool {
	return varType == common.VarTypeReference
}

// 是否是数组
func IsArray(varType string) bool {
	return len(varType) > 2 && varType[0:2] == "[]"
}

// 是否是map
func IsMap(varType string) bool {
	return len(varType) > 5 && varType[0:4] == "map["
}

// 是否是其他类型
func IsOther(varType string) bool {
	return varType == common.VarTypeOther
}
