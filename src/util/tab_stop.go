package util

func TabStop(line string, number uint8) string {
	if number == 0 {
		return line
	} else {
		return "    " + TabStop(line, number-1)
	}
}
