package util

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

func ReadFile(path string) (data []byte) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalln("无法读取", path)
	}
	return
}

func ReadJsonFile(path string, target interface{}) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalln("无法读取", path)
	}
	if err := json.Unmarshal(data, target); err != nil {
		log.Fatalln("无法解析文件", path, err.Error())
	}
}

func WriteFile(path string, data []byte) {
	if err := ioutil.WriteFile(path, data, 0644); err != nil {
		log.Fatalln("拷贝文件失败", path, err.Error())
	}
}
