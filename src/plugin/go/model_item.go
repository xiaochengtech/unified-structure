package golang

import (
	"fmt"
	"strings"

	"gitee.com/xiaochengtech/unified-structure/src/common"
	"gitee.com/xiaochengtech/unified-structure/src/util"
)

// 默认的模型字段格式
func modelItem(ctx common.Context) string {
	if util.IsExtend(ctx.GetFieldVarType()) {
		return ctx.Field.GetRefName()
	}
	varType := convertComplexType(ctx.GetFieldVarType(), ctx.Field)
	labels := []string{
		fmt.Sprintf("json:\"%s\"", ctx.Field.Json),
	}
	if ctx.Field.ExtGo.HasGorm() {
		labels = append(labels, fmt.Sprintf("gorm:\"%s\"", ctx.Field.ExtGo.Gorm))
	}
	label := strings.Join(labels, " ")
	return fmt.Sprintf("%s %s `%s` // %s", ctx.Field.Name, varType, label, ctx.Field.GetComment())
}

// Json类型模型字段的Get方法
func modelItemGetFunc(ctx common.Context) (lines []string) {
	lines = append(lines, fmt.Sprintf("func (m %s) Get%s() (item %s, err error) {", ctx.File.Name, ctx.Field.Name, ctx.Field.GetRefName()))
	lines = append(lines, fmt.Sprintf("err = json.Unmarshal([]byte(m.%s), &item)", ctx.Field.Name))
	lines = append(lines, "return")
	lines = append(lines, "}")
	return
}
