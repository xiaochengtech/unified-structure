package golang

import (
	"fmt"
	"strings"

	"gitee.com/xiaochengtech/unified-structure/src/common"
)

// 默认的常量字段格式
func constantItemDefault(ctx common.Context) string {
	varName := fmt.Sprintf("%s%s", ctx.File.Name, ctx.Field.Name)
	varType := convertBasicType(ctx.GetFieldVarType())
	if varType == "string" {
		return fmt.Sprintf("%s = \"%s\" // %s", varName, ctx.Field.Value, ctx.Field.GetComment())
	} else if (strings.Contains(varType, "int") && varType != "interface{}") || strings.Contains(varType, "float") {
		return fmt.Sprintf("%s = %s(%s) // %s", varName, varType, ctx.Field.Value, ctx.Field.GetComment())
	} else {
		return fmt.Sprintf("%s = %s // %s", varName, ctx.Field.Value, ctx.Field.GetComment())
	}
}

// IOTA常量第一个字段格式
func constantItemIotaFirst(ctx common.Context) string {
	varName := fmt.Sprintf("%s%s", ctx.File.Name, ctx.Field.Name)
	varType := convertBasicType(ctx.GetFieldVarType())
	var offset string
	if ctx.File.Iota.Offset > 0 {
		offset = fmt.Sprintf("+ %d", ctx.File.Iota.Offset)
	} else if ctx.File.Iota.Offset < 0 {
		offset = fmt.Sprintf("%d", ctx.File.Iota.Offset)
	} else {
		offset = ""
	}
	return fmt.Sprintf("%s %s = iota %s // %s", varName, varType, offset, ctx.Field.GetComment())
}

// IOTA常量非第一个的字段格式
func constantItemIotaOther(ctx common.Context) string {
	varName := fmt.Sprintf("%s%s", ctx.File.Name, ctx.Field.Name)
	return fmt.Sprintf("%s // %s", varName, ctx.Field.GetComment())
}
