package golang

import (
	"fmt"
	"log"
	"strings"

	"gitee.com/xiaochengtech/unified-structure/src/common"
	"gitee.com/xiaochengtech/unified-structure/src/util"
)

// 基础类型转换
func convertBasicType(varType string) string {
	switch varType {
	case common.VarTypeString, common.VarTypeJson:
		return "string"
	case common.VarTypeUint8, common.VarTypeUint16, common.VarTypeUint32, common.VarTypeUint64, common.VarTypeInt8, common.VarTypeInt16, common.VarTypeInt32, common.VarTypeInt64, common.VarTypeFloat32, common.VarTypeFloat64:
		return varType
	case common.VarTypeBool:
		return "bool"
	case common.VarTypeAny:
		return "interface{}"
	default:
		return ""
	}
}

// 复杂类型转换
func convertComplexType(varType string, item common.Field) string {
	if util.IsOther(varType) {
		return getReferName(item)
	} else if util.IsArray(varType) {
		innerType := varType[2:]
		return "[]" + convertComplexType(innerType, item)
	} else if util.IsMap(varType) {
		mapParts := strings.Split(varType, "]")
		if len(mapParts) != 2 || len(mapParts[0]) <= 4 {
			log.Fatalln("错误的map类型", item.Name)
		}
		keyType := convertBasicType(mapParts[0][4:])
		valueType := convertBasicAndReferenceType(mapParts[1], item)
		return fmt.Sprintf("map[%s]%s", keyType, valueType)
	} else {
		return convertBasicAndReferenceType(varType, item)
	}
}

func convertBasicAndReferenceType(varType string, item common.Field) string {
	basicType := convertBasicType(varType)
	if len(basicType) != 0 {
		return basicType
	}
	if util.IsReference(varType) {
		return getReferName(item)
	}
	log.Fatalln("错误的最终类型" + varType)
	return ""
}

func getReferName(item common.Field) string {
	if len(item.Refer.Name) == 0 {
		log.Fatalln("refer.name必须存在", item.Name)
	}
	return item.Refer.Name
}
