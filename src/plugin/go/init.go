package golang

import (
	"fmt"
	"log"
	"strings"

	"gitee.com/xiaochengtech/unified-structure/src/common"
)

// 注册语言处理插件
func init() {
	common.Plugins[common.LanguageGo] = new(Process)
}

// 需要实现common.Plugin的结构体
type Process struct{}

// 获取生成文件的内容
func (m *Process) GetMainFile(ctx common.Context) (filename string, content string) {
	packageName := ctx.Dir.ExtGo.GetPackageName()
	// 生成内容
	lines := make([]string, 0)
	if ctx.File.HasComment() {
		lines = append(lines, fmt.Sprintf("// %s", ctx.File.Comment))
	}
	lines = append(lines, fmt.Sprintf("package %s", packageName))
	if ctx.Dir.IsConstant() {
		lines = append(lines, constantFile(ctx)...)
	} else if ctx.Dir.IsModel() {
		lines = append(lines, modelFile(ctx)...)
	} else {
		log.Fatalln("模式错误", ctx.Dir.Mode)
	}
	// 结果
	filename = ctx.CurFileName + ".go"
	content = strings.Join(lines, "\n")
	return
}

// 获取其他文件的内容
func (m *Process) GetOtherFile(ctx common.Context) (filename string, content string) {
	return
}

// 是否可以拷贝
func (m *Process) CanCopy(ctx common.Context, fileName string) bool {
	for _, ext := range ctx.Dir.ExtGo.CopyExt {
		if strings.Contains(fileName, ext) {
			return true
		}
	}
	return false
}
