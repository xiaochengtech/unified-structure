package golang

import (
	"fmt"

	"gitee.com/xiaochengtech/unified-structure/src/common"
)

// 处理常量文件
func constantFile(ctx common.Context) (lines []string) {
	if ctx.File.HasField() {
		lines = append(lines, "const (")
		for i, item := range ctx.File.List {
			ctx.Field = item
			if ctx.File.Iota.Enabled {
				if i == 0 {
					lines = append(lines, constantItemIotaFirst(ctx))
				} else {
					lines = append(lines, constantItemIotaOther(ctx))
				}
			} else {
				lines = append(lines, constantItemDefault(ctx))
			}
		}
		lines = append(lines, ")")
	}
	if ctx.File.ExtGo.EnableSet {
		lines = append(lines, fmt.Sprintf("func Get%sLabel(v %s) string {", ctx.File.Name, ctx.File.VarType))
		lines = append(lines, "switch v {")
		for _, item := range ctx.File.List {
			varName := fmt.Sprintf("%s%s", ctx.File.Name, item.Name)
			lines = append(lines, fmt.Sprintf("case %s:", varName))
			lines = append(lines, fmt.Sprintf("return \"%s\"", item.GetComment()))
		}
		lines = append(lines, "}")
		lines = append(lines, "return \"\"")
		lines = append(lines, "}")
	}
	return
}
