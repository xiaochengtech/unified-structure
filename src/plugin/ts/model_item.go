package ts

import (
	"fmt"
	"log"

	"gitee.com/xiaochengtech/unified-structure/src/common"
	"gitee.com/xiaochengtech/unified-structure/src/util"
)

// 默认的模型字段格式
func modelItem(ctx common.Context) (lines []string, needRefer bool) {
	if util.IsExtend(ctx.GetFieldVarType()) {
		log.Fatalln("扩展属性应在文件级处理" + ctx.Field.Name)
	}
	if ctx.Field.IsJsonIgnore() {
		return
	}
	varType, needRefer := convertComplexType(ctx.GetFieldVarType(), ctx.Field)
	lines = append(lines, fmt.Sprintf("/** %s */", ctx.Field.GetComment()))
	lines = append(lines, fmt.Sprintf("%s: %s;", ctx.Field.Json, varType))
	return
}
