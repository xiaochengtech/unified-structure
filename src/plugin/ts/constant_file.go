package ts

import (
	"fmt"

	"gitee.com/xiaochengtech/unified-structure/src/common"
	"gitee.com/xiaochengtech/unified-structure/src/util"
)

// 处理常量文件
func constantFile(ctx common.Context) (lines []string) {
	for i, item := range ctx.File.List {
		if ctx.File.Iota.Enabled {
			item.Value = fmt.Sprintf("%d", int64(i)+ctx.File.Iota.Offset)
		}
		ctx.Field = item
		lines = append(lines, constantItemDefault(ctx)...)
	}
	if ctx.File.ExtTs.EnableSet {
		lines = append(lines, "")
		lines = append(lines, "/** 合集 */")
		lines = append(lines, "export const Set = [")
		for _, item := range ctx.File.List {
			ctx.Field = item
			lines = append(lines, util.TabStop(constantItemSet(ctx), 1))
		}
		lines = append(lines, "];")
	}
	return
}
