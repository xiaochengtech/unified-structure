package ts

import (
	"fmt"

	"gitee.com/xiaochengtech/unified-structure/src/common"
)

// 默认的常量字段格式
func constantItemDefault(ctx common.Context) (lines []string) {
	varType := convertBasicType(ctx.GetFieldVarType())
	lines = append(lines, fmt.Sprintf("/** %s */", ctx.Field.GetComment()))
	if varType == "string" {
		lines = append(lines, fmt.Sprintf("export const %s = \"%s\";", ctx.Field.Name, ctx.Field.Value))
	} else {
		lines = append(lines, fmt.Sprintf("export const %s = %s;", ctx.Field.Name, ctx.Field.Value))
	}
	return
}

// 集合的常量字段格式
func constantItemSet(ctx common.Context) string {
	return fmt.Sprintf("{ value: %s, label: \"%s\" },", ctx.Field.Name, ctx.Field.Label)
}
