package common

import (
	"log"
)

type Plugin interface {
	// 获取生成文件的内容
	GetMainFile(ctx Context) (filename string, content string)
	// 获取其他文件的内容
	GetOtherFile(ctx Context) (filename string, content string)
	// 是否可以拷贝
	CanCopy(ctx Context, fileName string) bool
}

var Plugins = map[string]Plugin{}

func GetPlugin(language string) (handle Plugin) {
	handle, ok := Plugins[language]
	if !ok {
		log.Fatalln("语言不支持", language)
	}
	return
}
