package common

// (Ts)目录整体配置信息的扩展参数
type DirectoryTs struct {
	CopyExt []string `json:"copy-ext"` // 可以拷贝的文件后缀名列表
}

// (Ts)文件配置信息的扩展参数
type FileTs struct {
	EnableSet bool `json:"enable-set"` // 导出展示标签集合
}

// (Ts)字段配置信息的扩展参数
type FieldTs struct {
}
