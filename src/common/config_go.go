package common

import (
	"log"
)

// (Go)目录整体配置信息的扩展参数
type DirectoryGo struct {
	CopyExt     []string `json:"copy-ext"`     // 可以拷贝的文件后缀名列表
	PackageName string   `json:"package-name"` // go的包名
}

func (m DirectoryGo) GetPackageName() string {
	if len(m.PackageName) == 0 {
		log.Fatalln("包名不能为空")
	}
	return m.PackageName
}

// (Go)文件配置信息的扩展参数
type FileGo struct {
	GormTableName string   `json:"gorm-table-name"` // GORM数据库模式时的表名
	ImportPkgs    []string `json:"import-pkgs"`     // 导入的包名列表
	EnableSet     bool     `json:"enable-set"`      // 是否导出获取label的函数
}

func (m FileGo) IsGormEnabled() bool {
	return len(m.GormTableName) > 0
}

func (m FileGo) HasImportPkgs() bool {
	return len(m.ImportPkgs) > 0
}

// (Go)字段配置信息的扩展参数
type FieldGo struct {
	Gorm string `json:"gorm"` // GORM特定属性
}

func (m FieldGo) HasGorm() bool {
	return len(m.Gorm) > 0
}
