package common

// 上下文信息
type Context struct {
	Dir         Directory // 目录信息
	File        File      // 文件信息
	Field       Field     // 字段信息
	CurFileName string    // 当前文件名称
}

func (m Context) GetFieldVarType() string {
	if len(m.Field.Type) > 0 {
		return m.Field.Type
	}
	return m.File.VarType
}
