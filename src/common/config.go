package common

import (
	"log"
)

// 目录整体配置信息
type Directory struct {
	Mode  string      `json:"mode"`   // 目录模式，常量还是模型
	ExtGo DirectoryGo `json:"ext-go"` // (Go)目录的扩展属性
	ExtTs DirectoryTs `json:"ext-ts"` // (Ts)目录的扩展属性
}

const (
	FileModeConstant = "constant" // 常量模式
	FileModeModel    = "model"    // 模型模式
)

func (m Directory) IsConstant() bool {
	return m.Mode == FileModeConstant
}

func (m Directory) IsModel() bool {
	return m.Mode == FileModeModel
}

// 文件的通用配置信息
type File struct {
	Name     string       `json:"name"`     // 模块名称
	Comment  string       `json:"comment"`  // 文件级注释
	Language []string     `json:"language"` // 导出的语言列表
	VarType  string       `json:"vartype"`  // 统一的变量类型，IOTA为必填，其他为选填，优先级不如字段的type
	Iota     IotaProperty `json:"iota"`     // (常量模式) 特定的IOTA自增数字模式
	List     []Field      `json:"list"`     // 常量或字段列表
	ExtGo    FileGo       `json:"ext-go"`   // (Go)目录的扩展属性
	ExtTs    FileTs       `json:"ext-ts"`   // (Ts)目录的扩展属性
}

type IotaProperty struct {
	Enabled bool  `json:"enable"` // 是否启用，默认为false
	Offset  int64 `json:"offset"` // 偏移量
}

func (m File) HasComment() bool {
	return len(m.Comment) > 0
}

func (m File) HasField() bool {
	return len(m.List) > 0
}

// 单个列表项的基础部分
type Field struct {
	Name    string        `json:"name"`    // 变量名
	Type    string        `json:"type"`    // 变量类型
	Json    string        `json:"json"`    // Json字段名
	Label   string        `json:"label"`   // 对应的展示内容
	Comment string        `json:"comment"` // 注释
	Value   string        `json:"value"`   // (mode=constant) 变量值
	Refer   ReferProperty `json:"refer"`   // (type=extend) 扩展属性
	ExtGo   FieldGo       `json:"ext-go"`  // (Go)目录的扩展属性
	ExtTs   FieldTs       `json:"ext-ts"`  // (Ts)目录的扩展属性
}

type ReferProperty struct {
	Name string `json:"name"` // 引用对象的名称
	File string `json:"file"` // 引用对象的文件名
}

func (m Field) IsJsonIgnore() bool {
	return m.Json == "-" || m.Json == ""
}

func (m Field) GetComment() string {
	if len(m.Comment) > 0 {
		return m.Comment
	} else {
		return m.Label
	}
}

func (m Field) GetRefName() string {
	if len(m.Refer.Name) == 0 {
		log.Fatalln("引用字段必须有名字", m.Name)
	}
	return m.Refer.Name
}

func (m Field) GetRefFile() string {
	if len(m.Refer.File) == 0 {
		log.Fatalln("引用字段必须有路径", m.Name)
	}
	return m.Refer.File
}
