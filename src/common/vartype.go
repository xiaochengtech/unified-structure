package common

const (
	// 基础类型，常量仅支持基础类型
	VarTypeAny     = "any"
	VarTypeString  = "string"
	VarTypeUint8   = "uint8"
	VarTypeUint16  = "uint16"
	VarTypeUint32  = "uint32"
	VarTypeUint64  = "uint64"
	VarTypeInt8    = "int8"
	VarTypeInt16   = "int16"
	VarTypeInt32   = "int32"
	VarTypeInt64   = "int64"
	VarTypeBool    = "bool"
	VarTypeFloat32 = "float32"
	VarTypeFloat64 = "float64"
	// (模型专有类型) 扩展类型，数组"[]xxx"和映射"map[xxx]xxx"单独解析(采用Go的语法标准)
	VarTypeJson      = "json"      // JSON字符串，对外表现为字符串，但是需添加Get函数，解析或设置到对应的结构体
	VarTypeExtend    = "extend"    // 继承类型，用于指定模型之间的继承关系
	VarTypeReference = "reference" // 引用类型，对应模型的层级引用关系，需要在Refer中指定引用的模块和文件信息
	VarTypeOther     = "other"     // 其他类型，需根据不同的语言插件，解析其他参数来确定
)
