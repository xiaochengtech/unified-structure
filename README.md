# unified-structure

这是一个用Go编写的，用于开发过程中，生成多端统一的常量和模型代码的工具。

采用了Json作为通用的格式，然后通过本工具，转换成对应不同语言的文件，这样每次修改Json文件之后，只要重新生成一下，就可以有全新同步的后端和前端代码了。

目前支持如下目标语言：

* Go
* TypeScript
* JavaScript：不直接支持，而是通过生成TypeScript，然后运行tsc进行转换。

### 测试样例

在`example/origin`中，有两个样例目录，`constant`是常量源文件目录，`model`是模型源文件目录。

运行`make test`后，生成的代码在`dist`中，分别生成Go和TypeScript代码。

具体样例的构建过程，参见`Makefile`。

### 使用方法

安装命令：

```shell
go get -u gitee.com/xiaochengtech/unified-structure

# 调用方式
`go env GOPATH`/bin/unified-structure
```

可执行文件位置为`$GOPATH/bin/unified-structure`，参数如下：

| 名称 | 必填 | 描述 |
| :---: | :---: | :---: |
| -src [SRC_DIR] | 是 | 源路径，路径下是一些Json文件，和其他类型的文件 |
| -dest [DEST_DIR] | 是 | 目标路径，生成目标语言的对应文件，以及从源路径拷贝了部分支持类型的文件 |
| -language [LANGUAGE] | 是 | 目标语言，目前支持`go`和`ts`(TypeScript) |

源路径下，必须包含一个`index.json`，用于声明一些Package级别的信息，其他Json文件各自是一个小的模块，如果有非Json文件，则根据已有语言选择性拷贝到目标路径中。

### `index.json`格式

完整格式如下：

```json
{
    "mode": "(必填) 目录模式，常量(constant)还是模型(model)",
    "ext-go": {
        "copy-ext": [],
        "package-name": "go的包名"
    },
    "ext-ts": {
        "copy-ext": [],
    }
}
```

其中`ext-xxx`是不同语言的额外配置，可以选择性填写。`copy-ext`为可以拷贝的文件的后缀名列表。

目前只支持整个目录都是常量，或者都是模型的方式。不支持混合方式。

### 普通Json文件格式

不管常量还是模型，格式都是如下：

```json
{
    "name": "(必填) 模块名称，Go中必须首字母大写",
    "comment": "(选填) 文件级注释",
    "language": "(必填) 支持导出的语言列表",
    "vartype": "统一的变量类型，IOTA为必填，其他为选填，优先级不如字段的type",
    "iota": {
        "enable": "是否启用，默认为false，整个iota",
        "offset": "偏移量"
    },
    "list": [],
    "ext-go": {
        "gorm-table-name": "GORM数据库模式时的表名",
        "import-pkgs": "导入的包名列表，字符串数组",
        "enable-set": "是否导出获取label标签的函数"
    },
    "ext-ts": {
        "enable-set": "是否导出label标签的集合"
    }
}
```

这里`iota`整个对象都是选填，只在常量模式生效，表示特定的IOTA自增数字模式，参见Go的语法。

`list`是常量或字段的列表，数组内部元素参见下面字段格式。

### 字段格式

常量或者模型的字段格式也是统一的，如下所示：

```json
{
    "name": "(必填) 变量名",
    "type": "(选填) 变量类型",
    "json": "(必填) Json字段名",
    "label": "(选填) 对应的展示内容",
    "comment": "(选填) 注释",
    "value": "(常量模式) 变量值",
    "refer": {
        "name": "引用对象的名称",
        "file": "引用对象的文件名"
    },
    "ext-go": {
        "gorm": "GORM特定属性"
    },
    "ext-ts": {
    }
}
```

其中`refer`是扩展类型(extend)字段的信息。

字段类型的支持列表，请参见`src/common/vartype.go`。